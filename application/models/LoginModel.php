<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends MY_Model {

    public function checkUserLogin($data = array())
    {
        $where = sprintf(
            "(username = '%s' OR email = '%s') AND password = '%s'",
            $data['username'],
            $data['username'],
            md5(sha1($data['password'], false))
        );
        $data = $this->db->select()
                         ->where($where)
                         ->get('account');
        if (!empty($data->result())) {
            $data = $data->result();
            return $this->__setSession($data[0]);
        }
        return false;
    }

    private function __setSession($data)
    {
        $newdata = array(
            'id' => $data->id,
            'firstname' => $data->firstname,
            'lastname' => $data->lastname,
            'username' => $data->username,
            'email' => $data->email,
            'account_type' => $data->account_type,
            'birthdate' => $data->birthdate,
            'created' => $data->created,
            'modified' => $data->modified,
            'is_loggedin' => true,
        );

        $this->session->set_userdata($newdata);
        return $this->session->userdata('is_loggedin');
    }

}
