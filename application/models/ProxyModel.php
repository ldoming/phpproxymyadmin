<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProxyModel extends MY_Model {

    public function checkPathAlreadyExist()
    {
        $query = $this->db->get('proxy_configuration');
        if ($query->num_rows() > 0) {
            return $query->result()[0]->user_lists_path;
        } else {
            return false;
        }
    }

    public function setProxyConfiguration($path = null)
    {
        $date = date('Y-m-d H:i:s');
        $data = array(
            'user_lists_path' => $path,
            'created' => $date,
            'modified' => $date,
        );
        $result = $this->db->insert('proxy_configuration', $data);
        if ($result) {
            $lines = file($path, FILE_IGNORE_NEW_LINES);
            if (!empty($lines)) {
                foreach ($lines as $key => $line) {
                    $line = explode(':', $line);
                    if (empty($line[0]) && empty($line[1])) {
                        continue;
                    }
                    $data = array(
                        'username' => str_replace(PHP_EOL, "", $line[0]),
                        'password' => str_replace(PHP_EOL, "", $line[1]),
                        'created_by' => $this->session->userdata('id'),
                        'modified_by' => $this->session->userdata('id'),
                        'reason' => 'Auto add on configuration',
                        'created' => $date,
                        'modified' => $date,
                    );
                $this->db->insert('proxy_account', $data);
                }
            }
            return true;
        }
        return false;
    }

    public function updateProxyConfiguration($path = null)
    {
        if ($this->deleteProxyConfiguration()) {
            return $this->setProxyConfiguration($path);
        }
        return false;
    }

    public function deleteProxyConfiguration()
    {
        if ($this->db->truncate('proxy_configuration')) {
            return $this->db->truncate('proxy_account');
        }
        return false;
    }

    public function getProxyAccounts()
    {
        $this->db->select('ProxyAccount.*, CreatorAccount.username AS creator, ModifierAccount.username as modifier');
        $this->db->from('proxy_account AS ProxyAccount');
        $this->db->join('account AS CreatorAccount', 'CreatorAccount.id = ProxyAccount.created_by', 'left');
        $this->db->join('account AS ModifierAccount', 'ModifierAccount.id = ProxyAccount.modified_by', 'left');
        $this->db->order_by("username", "asc"); 
        $this->db->order_by("firstname", "asc"); 
        $this->db->where('ProxyAccount.delete_flag', false);
        $query = $this->db->get();
        return $query->result();
    }

    public function addProxyAccount()
    {
        $file_path = $this->checkPathAlreadyExist();
        if (!$file_path) {
            return false;
        }
        $date = date('Y-m-d H:i:s');
        $input = $this->input->post();
        $data = array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'username' => $input['username'],
            'password' => $input['password'],
            'reason' => $input['reason'],
            'created_by' => $this->session->userdata('id'),
            'modified_by' => $this->session->userdata('id'),
            'created' => $date,
            'modified' => $date,
        );
        $result = $this->db->insert('proxy_account', $data);
        if ($result) {
            return $this->__writeProxyAccountToFile($input['username'], $input['password']);
        }
        return false;
    }

    public function updateProxyAccount($id = null)
    {
        $file_path = $this->checkPathAlreadyExist();
        if (!$file_path) {
            return false;
        }
        $date = date('Y-m-d H:i:s');
        $input = $this->input->post();
        $data = array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'username' => $input['username'],
            'password' => $input['password'],
            'reason' => $input['reason'],
            'modified_by' => $this->session->userdata('id'),
            'modified' => $date,
        );
        $this->db->where('id', $id);
        $result = $this->db->update('proxy_account', $data);
        if ($result) {
            $new = $input['username'] . ':' . $input['password'];
            $old = $input['username_original'] . ':' . $input['password_original'];
            return $this->__updateProxyAccountToFile($new, $old);
        }
        return false;
    }

    public function deleteProxyAccount($id)
    {

        $query = $this->db->get_where('proxy_account', array('id' => $id));
        $query = $query->result()[0];
        $result = $this->__removeProxyAccountToFile($query->username, $query->password);
        if ($result) {
            /*
            //Soft Delete Feature
            $this->db->set('delete_flag', true);
            $this->db->where('id', $id);
            $result = $this->db->update('proxy_account');
            */
            return $this->db->delete('proxy_account', array('id' => $id));
        }
        return false;
    }

    public function resynchronizeAccounts()
    {
        $date = date('Y-m-d H:i:s');
        $file_path = $this->checkPathAlreadyExist();
        if ($file_path) {
            $file = fopen($file_path, "r");
            $data = array();
            if ($file) {
                while (!feof($file)) {
                    $line = fgets($file);
                    $line = explode(':', $line);
                    if (empty($line[0]) || empty($line[1])) {
                        continue;
                    }
                    $this->db->select('id');
                    $this->db->where('username', str_replace(PHP_EOL, "", $line[0]));
                    $this->db->where('password', str_replace(PHP_EOL, "", $line[1]));
                    $query = $this->db->get('proxy_account');
                    if (empty($query->result())) {
                        $data[] = array(
                            'username' => $line[0],
                            'password' => $line[1],
                            'created_by' => $this->session->userdata('id'),
                            'modified_by' => $this->session->userdata('id'),
                            'delete_flag' => false,
                            'reason' => 'Auto add on configuration',
                            'created' => $date,
                            'modified' => $date,
                        );
                    }
                }
                fclose($file);
            }
            $result = (empty($data)) ? true : $this->db->insert_batch('proxy_account', $data);
            if ($result) {
                $this->db->select('username, password');
                $this->db->where('delete_flag', false);
                $query = $this->db->get('proxy_account');
                return $this->__regenerateProxyAccountToFile($query->result());
            }
        }
        return false;
    }

    public function getUserCredential($id)
    {
        $query = $this->db->get_where('proxy_account', array('id' => $id, 'delete_flag' => false));
        return $query->result()[0];
    }

    /**
     *
     * PRIVATE METHODS BELOW
     *
     **/

    private function __writeProxyAccountToFile($username = null, $password = null)
    {
        $file_path = $this->checkPathAlreadyExist();
        #$txt = PHP_EOL . $username . ':' . $password;
        $txt = $username . ':' . $password;
        return file_put_contents($file_path, $txt . PHP_EOL, FILE_APPEND);
    }

    private function __updateProxyAccountToFile($new = null, $old = null)
    {
        if (empty($new) || empty($old)) {
            return false;
        }
        $file_path = $this->checkPathAlreadyExist();
        $lines = file($file_path, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $key => $line) {
            if ($line === $old) {
                $lines[$key] = $new;
            }
        }
        $data = implode(PHP_EOL, array_values($lines));
        return file_put_contents($file_path, $data);
    }

    private function __removeProxyAccountToFile($username = null, $password = null)
    {
        $file_path = $this->checkPathAlreadyExist();
        $lines = file($file_path, FILE_IGNORE_NEW_LINES);
        $remove = $username . ':' . $password;
        foreach ($lines as $key => $line) {
            if ($line === $remove) {
                unset($lines[$key]);
            }
        }
        $data = implode(PHP_EOL, array_values($lines));
        return file_put_contents($file_path, $data);
    }

    private function __regenerateProxyAccountToFile($accounts)
    {

        $file_path = $this->checkPathAlreadyExist();
        $lines = array();
        foreach ($accounts as $account) {
            $lines[] = $account->username . ':' . $account->password;
        }
        $data = implode(PHP_EOL, array_values($lines));
        return file_put_contents($file_path, $data);
    }
}
