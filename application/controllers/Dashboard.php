<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    /**
     * Index Page
     */
    public function index()
    {
        $this->load->view('template/header');
        $this->load->view('dashboard/index');
        $this->load->view('template/footer');
    }
}
