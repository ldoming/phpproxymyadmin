<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proxy extends MY_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProxyModel');
    }

    /**
     * Index Page
     */
    public function index()
    {
        $data['proxy_accounts'] = $this->ProxyModel->getProxyAccounts();
        $this->load->view('template/header');
        $this->load->view('proxy/index', $data);
        $this->load->view('template/footer');
    }

    public function proxyConfiguration()
    {
        $data['path'] = $this->ProxyModel->checkPathAlreadyExist();
        $this->load->view('template/header');
        $this->load->view('proxy/proxyConfiguration', $data);
        $this->load->view('template/footer');
    }

    public function addConfiguration()
    {
        if (!empty($this->input->post())) {
            if ($this->form_validation->run('addProxySettings') == true) {
                $result = false;
                if (is_writable($this->input->post('proxy_path'))) {
                    $result = $this->ProxyModel->setProxyConfiguration($this->input->post('proxy_path'));
                } else {
                    $this->session->set_flashdata('error', 'Cannot open file, It may not exist or file permission reasons!');
                    redirect('/proxy/proxyConfiguration');
                }

                if ($result) {
                    $this->session->set_flashdata('success', 'Configuration added successfully.');
                    redirect('/proxy');
                } else {
                    $this->session->set_flashdata('error', 'Error adding configuration.');
                }
            }
        }
        $this->proxyConfiguration();
    }

    public function updateConfiguration()
    {
        if (!empty($this->input->post())) {
            if ($this->form_validation->run('updateProxySettings') == true) {
                $file = read_file($this->input->post('proxy_path_update'));
                $result = false;
                if ($file) {
                    $result = $this->ProxyModel->updateProxyConfiguration($this->input->post('proxy_path_update'));
                } else {
                    $this->session->set_flashdata('error', 'Cannot open file, It may not exist or file permission reasons!');
                }

                if ($result) {
                    $this->session->set_flashdata('success', 'Configuration updated successfully.');
                    redirect('/proxy');
                } else {
                    $this->session->set_flashdata('error', 'Error updating configuration.');
                }
            }
        }
        $this->proxyConfiguration();
    }

    public function deleteConfiguration()
    {
        if ($this->ProxyModel->deleteProxyConfiguration()) {
            $this->session->set_flashdata('success', 'Configuration deleted successfully');
        } else {
            $this->session->set_flashdata('error', 'Error deleting configuration. Please try again.');
        }
        redirect('/proxy');
    }

    public function addUserCredentials()
    {
        if (!empty($this->input->post())) {
            if ($this->form_validation->run('addProxyAccount') == true) {
                if ($this->ProxyModel->addProxyAccount()) {
                    $this->session->set_flashdata('success', 'Account added successfully');
                    redirect('/proxy');
                } else {
                    $this->session->set_flashdata('error', 'Error adding account. Please check if file path already exist! Please try again!');
                }
            }
        }
        $this->load->view('template/header');
        $this->load->view('proxy/addUserCredentials');
        $this->load->view('template/footer');
    }

    public function updateUserCredentials($id = null)
    {
        if (empty($id)) {
            $this->session->set_flashdata('error', 'ID not specified. Please try again!');
            redirect('/proxy');
        }
        if (!empty($this->input->post())) {
            $data = $this->input->post();
            if ($data['firstname_original'] != $data['firstname']) {
                $this->form_validation->set_rules('firstname', 'Firstname', 'required|is_unique[proxy_account.firstname]');
            }
            if ($data['lastname_original'] != $data['lastname']) {
                $this->form_validation->set_rules('lastname', 'Lastname', 'required|is_unique[proxy_account.lastname]');
            }
            if ($data['username_original'] != $data['username']) {
                $this->form_validation->set_rules('username', 'Username', 'required|is_unique[proxy_account.username]');
            }
            if ($this->form_validation->run('updateProxyAccount') == true) {
                if ($this->ProxyModel->updateProxyAccount($id)) {
                    $this->session->set_flashdata('success', 'Account updated successfully');
                    redirect('/proxy');
                } else {
                    $this->session->set_flashdata('error', 'Error updating account. Please try again!');
                }
            }
        }
        $data['original'] = $this->ProxyModel->getUserCredential($id);
        $data['hidden'] = array(
            'firstname_original' => $data['original']->firstname,
            'lastname_original' => $data['original']->lastname,
            'username_original' => $data['original']->username,
            'password_original' => $data['original']->password,
        );
        $this->load->view('template/header');
        $this->load->view('proxy/updateUserCredentials', $data);
        $this->load->view('template/footer');
    }

    public function deleteUserCredential($id = null)
    {
        if (empty($id)) {
            $this->session->set_flashdata('error', 'ID not provided');
            redirect('/proxy');
        }

        if ($this->ProxyModel->deleteProxyAccount($id)) {
            $this->session->set_flashdata('success', 'Account deleted successfully');
        } else {
            $this->session->set_flashdata('error', 'Error deleting account. Please try again!');
        }
        redirect('/proxy');
    }

    public function resynchronizeAccounts()
    {
        if ($this->ProxyModel->resynchronizeAccounts()) {
            $this->session->set_flashdata('success', 'Resynchronize successfully');
        } else {
            $this->session->set_flashdata('error', 'Error resynchronizing account. Please try again!');
        }
        redirect('/proxy');
    }
}
