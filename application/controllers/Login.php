<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    /**
     * Login main page
     */
    public function index()
    {
        //Check if user is logged in
        if ($this->session->userdata('is_loggedin')
            && $this->router->fetch_class() === 'login') {
            redirect('/dashboard');
        }

        if (!empty($this->input->post())) {
            if ($this->form_validation->run('login') == true) {
                $this->load->model('LoginModel');
                if ($this->LoginModel->checkUserLogin($this->input->post())) {
                    redirect('/dashboard');
                } else {

                }
            }
        }
        $this->load->view('login/index');
    }

    public function logout()
    {
        $user_data = array(
            'id',
            'firstname',
            'lastname',
            'username',
            'email',
            'account_type',
            'birthdate',
            'created',
            'modified',
            'is_loggedin',
        );
        $this->session->unset_userdata($user_data);
        $this->session->sess_destroy();
        redirect('/login');
    }
}
