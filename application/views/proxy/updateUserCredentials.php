<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Update User Proxy Credentials</h1>
    </div>
</div>

<?php 
    $this->load->view('elements/proxy_buttons');
    $this->load->view('elements/flash_message');
?>

<div class="row top-buffer-20">
    <div class="col-lg-6">
        <?php echo form_open('', '', $hidden);?>
        <div class="form-group">
            <label>Firstname</label>
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="<?php echo (empty(set_value('firstname'))) ? $original->firstname : set_value('firstname'); ?>">
            <?php echo form_error('firstname', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Lastname</label>
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" value="<?php echo (empty(set_value('lastname'))) ? $original->lastname : set_value('lastname'); ?>">
            <?php echo form_error('lastname', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo (empty(set_value('username'))) ? $original->username : set_value('username'); ?>">
            <?php echo form_error('username', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo (empty(set_value('password'))) ? $original->password : set_value('password'); ?>">
            <?php echo form_error('password', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Reason for adding</label>
            <textarea class="form-control" name="reason"><?php echo (empty(set_value('reason'))) ? $original->reason : set_value('reason'); ?></textarea>
            <?php echo form_error('reason', '<span class="text-danger">', '</span>'); ?>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
        <?php echo form_close();?>
    </div>
</div>