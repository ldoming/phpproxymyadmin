<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Proxy Server</h1>
    </div>
</div>

<?php 
    $this->load->view('elements/proxy_buttons');
    $this->load->view('elements/flash_message');
?>

<div class="table-responsive top-buffer-20">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Username</th>
                <th>Password</th>
                <th>Reason</th>
                <th>Created By</th>
                <th>Modified By</th>
                <th>Created</th>
                <th>Modified</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($proxy_accounts as $proxy_account): ?>
            <tr>
                <td><?php echo $proxy_account->firstname; ?></td>
                <td><?php echo $proxy_account->lastname; ?></td>
                <td><?php echo $proxy_account->username; ?></td>
                <td><?php echo $proxy_account->password; ?></td>
                <td><?php echo $proxy_account->reason; ?></td>
                <td><?php echo $proxy_account->creator; ?></td>
                <td><?php echo $proxy_account->modifier; ?></td>
                <td><?php echo $proxy_account->created; ?></td>
                <td><?php echo $proxy_account->modified; ?></td>
                <td>
                    <a href="/proxy/updateUserCredentials/<?php echo $proxy_account->id; ?>">
                        <button type="button" class="btn btn-info">Update</button>
                    </a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProxyAccount" onclick="getAccountId(<?php echo $proxy_account->id; ?>)">
                        Delete
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


<!-- MODAL AREA-->

<!-- Delete modal-->
<div class="modal fade" id="deleteProxyAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Delete Proxy Account</h4>
            </div>
            <div class="modal-body">
            <p>Are you sure want to delete this account?<p>
            <p class="text-danger"><strong>Note:</strong> This will permanently delete account from the database.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a id="deleteModalButton">
                    <button type="button" class="btn btn-danger">Delete</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End delete modal-->

<script type="text/javascript">
    
    var getAccountId = function(id) {
        $('#deleteModalButton').attr('href', '/proxy/deleteUserCredential/' + id);
    }

</script>