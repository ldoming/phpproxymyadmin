<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Proxy Configuration</h1>
    </div>
</div>

<?php 
    $this->load->view('elements/proxy_buttons');
    $this->load->view('elements/flash_message');
?>

<?php if ($path) : ?>
    <div class="row top-buffer-50">
        <div class="col-lg-6">
            <span class="text-success"><?php echo $path; ?></span> 
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#updateProxySetting">
                Update
            </button>
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProxySetting">
                Delete
            </button>
        </div>
    </div>
<?php else :?>
    <?php echo form_open('/proxy/addConfiguration'); ?>
    <div class="row top-buffer-20">
        <div class="col-lg-6">
            <div class="input-group">
                <input type="text" class="form-control" name="proxy_path" placeholder="Proxy Passwd Path" required>
                <span class="input-group-btn">
                    <button class="btn btn-success" type="submit">Submit</button>
                </span>
            </div>
            <?php echo form_error('proxy_path', '<span class="text-danger">', '</span>'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
<?php endif; ?>


<!-- MODAL AREA-->

<!-- Delete modal-->
<div class="modal fade" id="deleteProxySetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Delete Proxy Settings</h4>
            </div>
            <div class="modal-body">
            <p>Are you sure want to delete proxy settings?</p>
            <p class="text-danger">
                <strong>Warning!!!</strong> This process will delete all the record in the database.
            </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="/proxy/deleteConfiguration">
                    <button type="button" class="btn btn-danger">Delete</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End delete modal-->

<!-- Update modal-->
<div class="modal fade" id="updateProxySetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Update Proxy Settings</h4>
            </div>
            <div class="modal-body">
            <?php $attributes = array('id' => 'updateProxySettingConfirmationForm');?>
            <?php echo form_open('/proxy/updateConfiguration', $attributes);?>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" name="proxy_path_update" class="form-control" placeholder="Proxy Passwd Path">
                        <?php echo form_error('proxy_path_update', '<span class="text-danger">', '</span>'); ?>
                    </div>
                </div>
            <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#updateProxySettingConfirmation">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- End update modal-->

<!-- Update confirmation modal-->
<div class="modal fade" id="updateProxySettingConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Update Proxy Settings</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to update proxy settings?</p>
                <p class="text-danger">
                    <strong>Warning!!!</strong> This process will update all the record in the database based on the new path.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-info" id="submitUpdateProxySettingFormButton">
                    Update
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End update modal-->


<script type="text/javascript">
    $('#submitUpdateProxySettingFormButton').click(function(){
        $('#updateProxySettingConfirmationForm').submit();
    });

    <?php if (!empty(form_error('proxy_path_update'))) :?>
        $('#updateProxySetting').modal('toggle');
    <?php endif; ?>

</script>