<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add User Proxy Credentials</h1>
    </div>
</div>

<?php 
    $this->load->view('elements/proxy_buttons');
    $this->load->view('elements/flash_message');
?>

<div class="row top-buffer-20">
    <div class="col-lg-6">
        <?php echo form_open();?>
        <div class="form-group">
            <label>Firstname</label>
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="<?php echo set_value('firstname'); ?>">
            <?php echo form_error('firstname', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Lastname</label>
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" value="<?php echo set_value('lastname'); ?>">
            <?php echo form_error('lastname', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo set_value('username'); ?>">
            <?php echo form_error('username', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
            <?php echo form_error('password', '<span class="text-danger">', '</span>'); ?>
        </div>
        <div class="form-group">
            <label>Reason for adding</label>
            <textarea class="form-control" name="reason"><?php echo set_value('reason'); ?></textarea>
            <?php echo form_error('reason', '<span class="text-danger">', '</span>'); ?>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
        <?php echo form_close();?>
    </div>
</div>