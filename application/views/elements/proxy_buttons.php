<a href="/proxy"><button type="button" class="btn btn-warning">Proxy Account Lists</button></a>
<?php if ($this->session->account_type === 'Administrator') :?>
    <a href="/proxy/proxyConfiguration">
        <button type="button" class="btn btn-primary">
            Proxy Configuration
        </button>
    </a>
<?php endif;?>
<a href="/proxy/addUserCredentials">
    <button type="button" class="btn btn-success">
        Add Proxy User Credential
    </button>
</a>
<?php if ($this->session->account_type === 'Administrator') :?>
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#resyncProxyAccounts">Resynchronize Account List
</button>
<?php endif;?>

<!-- MODAL AREA-->

<!-- Delete modal-->
<div class="modal fade" id="resyncProxyAccounts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Resynchronize Account List</h4>
            </div>
            <div class="modal-body">
                <p class="text-danger">Are you sure want to resynchronize account list?</p>
                <p class="text-warning">This will recheck the listed account in the proxy path to update the database.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="/proxy/resynchronizeAccounts">
                    <button type="button" class="btn btn-danger">OK</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End delete modal-->