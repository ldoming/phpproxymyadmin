<?php if ($this->session->flashdata('error')) :?>
<div class="row top-buffer-20">
    <div class="col-lg-12">
        <?php echo '<div class="alert alert-danger" role="alert">' . $this->session->flashdata('error') . '</div>'; ?>
    </div>
</div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')) :?>
<div class="row top-buffer-20">
    <div class="col-lg-12">
        <?php echo '<div class="alert alert-success" role="alert">' . $this->session->flashdata('success') . '</div>'; ?>
    </div>
</div>
<?php endif; ?>