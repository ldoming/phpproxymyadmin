<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        //Check if user is logged in
        if (empty($this->session->userdata('is_loggedin'))
            && $this->router->fetch_class() !== 'login') {
            redirect('/login');
        }
    }
}
