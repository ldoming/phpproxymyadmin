<?php

$config = array(
    'login' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required',
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[8]|max_length[30]',
        ),
    ),
    'addProxySettings' => array(
        array(
            'field' => 'proxy_path',
            'label' => 'Proxy path',
            'rules' => 'required',
        ),
    ),
    'updateProxySettings' => array(
        array(
            'field' => 'proxy_path_update',
            'label' => 'Proxy path',
            'rules' => 'required',
        ),
    ),
    'addProxyAccount' => array(
        array(
            'field' => 'firstname',
            'label' => 'Firstname',
            #'rules' => 'required|is_unique[proxy_account.firstname]',
            'rules' => 'required',
        ),
        array(
            'field' => 'lastname',
            'label' => 'Lastname',
            #'rules' => 'required|is_unique[proxy_account.lastname]',
            'rules' => 'required',
        ),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|is_unique[proxy_account.username]',
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required',
        ),
        array(
            'field' => 'reason',
            'label' => 'Reason for adding',
            'rules' => 'required',
        ),
    ),
    'updateProxyAccount' => array(
        array(
            'field' => 'firstname',
            'label' => 'Firstname',
            'rules' => 'required',
        ),
        array(
            'field' => 'lastname',
            'label' => 'Lastname',
            'rules' => 'required',
        ),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required',
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required',
        ),
        array(
            'field' => 'reason',
            'label' => 'Reason for adding',
            'rules' => 'required',
        ),
    ),
);
