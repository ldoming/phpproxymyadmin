### What is this repository for? ###

* This repository is used to manage squid proxy accounts.
	- Create Account
	- Update Account
	- Delete Account
	- List Account
	- Reset Password

### How do I get set up? ###

* Clone the repository into your web root directory
	- git clone https://bitbucket.org/ldoming/phpproxymyadmin.git
	- git clone bitbucket.org:ldoming/phpproxymyadmin.git